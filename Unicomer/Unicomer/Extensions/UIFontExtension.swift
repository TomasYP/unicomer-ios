//
//  UIFont.swift
//  Visa
//
//  Created by Enrique Salazar Sixtos on 06/12/17.
//  Copyright © 2017 YellowPepper USA LLC. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    class func bbvaBentonSansMedium(size: CGFloat) -> UIFont? {
        return UIFont(name: "BentonSansBBVA-Medium", size: size)
    }
    
    class func bbvaBentonSansBook(size: CGFloat) -> UIFont? {
        return UIFont(name: "BentonSansBBVA-Book", size: size)
    }
}
