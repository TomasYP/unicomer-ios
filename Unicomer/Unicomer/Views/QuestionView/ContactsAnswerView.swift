//
//  ContactsAnswerView.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/26/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import UIKit

protocol ContactsAnswerViewDelegate: class {
    func displayContactsPickerController(forQuestion question: QuestionViewModel)
}

class ContactsAnswerView: UIView, AnswerView {
    
    //MARK: Outlets
    
    private var uploadButton: UIButton!
    
    private var contactsTextView: UITextView!
    
    //MARK: Properties
    
    var questionModel: QuestionViewModel? {
        didSet {
            if uploadButton == nil {
                adddOutlets()
            }
            updateTextViewWithModel()
        }
    }
    
    var hasFlexibleHeight: Bool {
        return false
    }
    
    weak var delegate: ContactsAnswerViewDelegate?
    
    //MARK: Constructor
    
    class func CreateAnswerViewFromModel(_ model: QuestionViewModel) -> ContactsAnswerView {
        let contactAnswerView = ContactsAnswerView(frame: CGRect(origin: .zero, size: CGSize(width: 0.0, height: 150)))
        contactAnswerView.questionModel = model
        return contactAnswerView
    }
    
    //MARK: Drawing
    
    private func adddOutlets() {
        addUploadButton()
        addTextView()
        setNeedsLayout()
    }
    
    private func updateTextViewWithModel() {
        contactsTextView.text = questionModel?.answer ?? ""
    }
    
    private func addUploadButton() {
        let uploadPhotoButton = UIButton(type: .contactAdd)
        addSubview(uploadPhotoButton)
        uploadPhotoButton.translatesAutoresizingMaskIntoConstraints = false
        uploadPhotoButton.addTarget(self, action: #selector(tappedSelectContacts(sender:)), for: .touchUpInside)
        let centerYConstraint = uploadPhotoButton.centerYAnchor.constraint(equalTo: centerYAnchor)
        let trailingConstraint = uploadPhotoButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8.0)
        NSLayoutConstraint.activate([centerYConstraint, trailingConstraint])
        uploadButton = uploadPhotoButton
    }
    
    private func addTextView() {
        let textView = UITextView(frame: CGRect(origin: .zero, size: .zero))
        textView.text = ""
        textView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(textView)
        let topConstraint = textView.topAnchor.constraint(equalTo: topAnchor, constant: 8.0)
        let leadingConstraint = textView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8.0)
        let trailingConstraint = textView.trailingAnchor.constraint(equalTo: uploadButton.leadingAnchor, constant: -8.0)
        let bottomConstraint = textView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 8.0)
        NSLayoutConstraint.activate([topConstraint, leadingConstraint, trailingConstraint, bottomConstraint])
        contactsTextView = textView
    }
    
    //MARK: Actions
    
    @objc func tappedSelectContacts(sender: UIButton) {
        guard let question = questionModel else { return }
        delegate?.displayContactsPickerController(forQuestion: question)
    }
    
    //MARK: View lifecycle
    
    private func setup() {
        backgroundColor = .clear
        isOpaque = false
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    //MARK: Answer view methods
    
    func gatherAnswerFromUserInput() throws -> String {
        let answer = questionModel?.answer ?? ""
        if answer == "" {
            throw AnswerViewError.insuficientData(message: "You need to select at least one option")
        } else {
            return answer
        }
    }
}

