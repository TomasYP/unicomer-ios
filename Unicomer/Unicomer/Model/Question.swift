//
//  Question.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/21/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import Foundation

struct Question {
    
    //MARK: Properties
    
    var question: String
    
    var length: Int
    
    var questionOptions: [QuestionOption]
    
    var quantity: Int
    
    var questionType: String
    
    var answer = ""
    
}

extension Question {
    
    //MARK: Initializers Mappable
    
    init(fromMappable mappable: QuestionMappable) {
        question = mappable.question ?? ""
        length = mappable.length ?? 0
        quantity = mappable.length ?? 0
        questionType = mappable.type ?? ""
        if let mappableOptions = mappable.options {
            questionOptions = mappableOptions.map { return QuestionOption(fromMappable: $0) }
        } else {
            questionOptions = []
        }
    }
    
    mutating func setAnswer(_ answer: String) {
        self.answer = answer
    }
    
}
