//
//  QuestionsViewController.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/21/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import UIKit

class QuestionsViewController: UIViewController {

    //MARK: Outlets
    
    @IBOutlet fileprivate weak var questionsDisplayView: QuestionsDisplayView!
    
    //MARK: Properties
    
    fileprivate var currentQuestion: QuestionViewModel?
    
    private var questionsDisplayViewHandler: QuestionsViewDelegateDataSource?
    
    //MARK: Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMockQuestions()
    }
    
    //MARK: Questions

    private func loadMockQuestions() {
        self.showActivityIndicator()
        QuestionsManager.shared.loadQuestions { [weak self] success, questions  in
            self?.dissmissActivityIndicator()
            guard let strongSelf = self else { return }
            strongSelf.load(questions: questions)
        }
    }
    
    private func load(questions: [Question]) {
        let questionsModel = QuestionsViewModel(fromQuestions: questions)
        questionsDisplayViewHandler = QuestionsViewDelegateDataSource(questions: questionsModel.questions, presentingController: self,
                                                                      questionsDisplayView: questionsDisplayView)
        self.questionsDisplayView.delegate = questionsDisplayViewHandler
        self.questionsDisplayView.dataSource = questionsDisplayViewHandler
        self.questionsDisplayView.realoadView()
    }
}
