//
//  UITextFieldExtension.swift
//  Visa
//
//  Created by MacBookAir01 on 20/12/17.
//  Copyright © 2017 YellowPepper USA LLC. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func setLeftPadding(paddignSize: CGFloat = Constants.defaultTextFieldPadding) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: paddignSize, height: 0))
        self.leftView = paddingView
        self.leftViewMode = UITextFieldViewMode.always
    }
}
