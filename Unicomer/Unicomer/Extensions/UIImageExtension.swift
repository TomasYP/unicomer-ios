//
//  UIImageExtension.swift
//  Visa
//
//  Created by Enrique Salazar Sixtos on 07/12/17.
//  Copyright © 2017 YellowPepper USA LLC. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    class func bbvaImageLogo() -> UIImage? {
        return UIImage(named: "BBVA_logo")
    }
    
    class func bbvaProfileImagePlaceholder() -> UIImage? {
        return UIImage(named: "userPlaceholder")
    }
    
    class func visaImageLogo() -> UIImage? {
        return UIImage(named: "visa_logo_icon")
    }
    
    class func tempBackgroundTrip() -> UIImage? {
        return UIImage(named: "defaultTripImage")
    }
    
    //MARK: - trips images
    class func homeLocationImage() -> UIImage? {
        return UIImage(named: "homePlace")
    }
    
    class func officeLocationImage() -> UIImage? {
        return UIImage(named: "officePlace")
    }
    
    class func locationIcon() -> UIImage? {
        return UIImage(named: "placeIcon")
    }
    
    class func checkCircle() -> UIImage? {
        return UIImage(named: "checkCircle")
    }
    
    class func crossIcon() -> UIImage? {
        return UIImage(named: "cross")
    }
    
    //MARK: Orientation
    
    func getImageWithOrientationFixed() -> UIImage {
        if self.imageOrientation == .up {
            return self
        }
        var transform: CGAffineTransform = .identity
        switch self.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: .pi)
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: .pi / 2)
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: self.size.height)
            transform = transform.rotated(by: -.pi / 2)
        default:
            break
        }
        
        switch self.imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: self.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: self.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        default:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height),
                                      bitsPerComponent: (self.cgImage)!.bitsPerComponent, bytesPerRow: 0,
                                      space: (self.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        ctx.concatenate(transform)
        
        switch self.imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: self.size.height, height: self.size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        }
        
        let cgimg: CGImage = ctx.makeImage()!
        let img: UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
}
