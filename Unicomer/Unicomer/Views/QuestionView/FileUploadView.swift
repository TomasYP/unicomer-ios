
//
//  FileUploadView.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/26/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import UIKit

protocol FileUploadViewDelegate: class {
    func displayPhotosFromLibrary(forQuestion question: QuestionViewModel)
    func displayCameraRoll(forQuestion question: QuestionViewModel)
}

class FileUploadView: UIView, AnswerView {

    //MARK: Outlets
    
    private var uploadButton: UIButton!
    
    private var uploadFromLibraryButton: UIButton!
    
    private var imageView: UIImageView!
    
    weak var delegate: FileUploadViewDelegate?
    
    //MARK: Properties
    
    var questionModel: QuestionViewModel? {
        didSet {
            if uploadButton == nil {
                adddOutlets()
            }
            populateWithModelData()
        }
    }
    
    var hasFlexibleHeight: Bool {
        return false
    }
    
    //MARK: Constructor
    
    class func CreateAnswerViewFromModel(_ model: QuestionViewModel) -> FileUploadView {
        let fileAnswerView = FileUploadView(frame: CGRect(origin: .zero, size: CGSize(width: 0.0, height: 300)))
        fileAnswerView.questionModel = model
        return fileAnswerView
    }
    
    //MARK: Drawing

    private func adddOutlets() {
        addUploadButton()
        addUploadFromLibraryButton()
        addImageView()
        setNeedsLayout()
    }
    
    private func addUploadButton() {
        let uploadPhotoButton = UIButton(type: .system)
        addSubview(uploadPhotoButton)
        uploadPhotoButton.setTitle("Take photo 📷", for: .normal)
        uploadPhotoButton.translatesAutoresizingMaskIntoConstraints = false
        uploadPhotoButton.addTarget(self, action: #selector(tappedTakePictureButton(sender:)), for: .touchUpInside)
        let centerXConstraint = uploadPhotoButton.centerXAnchor.constraint(equalTo: centerXAnchor)
        let bottomConstraint = uploadPhotoButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 8.0)
        NSLayoutConstraint.activate([centerXConstraint, bottomConstraint])
        uploadButton = uploadPhotoButton
    }
    
    private func addUploadFromLibraryButton() {
        let uploadLibraryButton = UIButton(type: .system)
        addSubview(uploadLibraryButton)
        uploadLibraryButton.setTitle("Select from photo library 📕", for: .normal)
        uploadLibraryButton.translatesAutoresizingMaskIntoConstraints = false
        uploadLibraryButton.addTarget(self, action: #selector(tappedUploadButton(sender:)), for: .touchUpInside)
        let centerXConstraint = uploadLibraryButton.centerXAnchor.constraint(equalTo: centerXAnchor)
        let bottomConstraint = uploadLibraryButton.bottomAnchor.constraint(equalTo: uploadButton.topAnchor, constant: 8.0)
        NSLayoutConstraint.activate([centerXConstraint, bottomConstraint])
        uploadFromLibraryButton = uploadLibraryButton
    }
    
    private func addImageView() {
        let imageView = UIImageView(frame: .zero)
        addSubview(imageView)
        self.imageView = imageView
        self.imageView.clipsToBounds = true
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        self.imageView.contentMode = .scaleAspectFit
        let topConstraint = self.imageView.topAnchor.constraint(equalTo: topAnchor, constant: 8.0)
        let leadingConstraint = self.imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8.0)
        let trailingConstraint = self.imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 8.0)
        let bottomConstraint = self.imageView.bottomAnchor.constraint(equalTo: self.uploadFromLibraryButton.topAnchor, constant: -8.0)
        NSLayoutConstraint.activate([topConstraint, leadingConstraint, trailingConstraint, bottomConstraint])
    }
    
    
    private func getImageFromBas64EncodedString(_ encodedImage: String) -> UIImage? {
        guard let imageData = Data(base64Encoded: encodedImage),
                let image = UIImage(data: imageData) else { return nil }
        return image
    }
    
    private func populateWithModelData() {
        guard let answer = questionModel?.answer, answer != "" else { return }
        if let image = getImageFromBas64EncodedString(answer) {
            imageView.image = image
            imageView.layoutIfNeeded()
        }
    }
    
    //MARK: Actions
    
    @objc func tappedUploadButton(sender: UIButton) {
        guard let question = questionModel else { return }
        delegate?.displayPhotosFromLibrary(forQuestion: question)
    }
    
    @objc func tappedTakePictureButton(sender: UIButton) {
        guard let question = questionModel else { return }
        delegate?.displayCameraRoll(forQuestion: question)
    }
    
    //MARK: View lifecycle
    
    private func setup() {
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    //MARK: Answer view methods
    
    func gatherAnswerFromUserInput() throws -> String {
        let answer = questionModel?.answer ?? ""
        if answer == "" {
            throw AnswerViewError.insuficientData(message: "You need to select at least one option")
        } else {
            return answer
        }
    }

}
