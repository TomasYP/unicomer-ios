//
//  File.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/21/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import Foundation

struct QuestionOption {
    
    //MARK: Properties
    
    var name: String
    
}

extension QuestionOption {
    
    //MARK: Initializers Mappable
    
    init(fromMappable mappable: QuestionOptionMappable) {
        name = mappable.name ?? ""
    }
    
}
