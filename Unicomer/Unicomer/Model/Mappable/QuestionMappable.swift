//
//  Question.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/21/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import Foundation
import ObjectMapper

struct QuestionsResponseMappable: Mappable {
    
    //MARK: Properties
    
    var questions: [QuestionMappable]?
    
    //MARK: Mapping
    
    init?(map: Map) {}
    
    init() {}
    
    mutating func mapping(map: Map) {
        questions <- map["questions"]
    }
    
}

struct QuestionMappable: Mappable {
    
    //MARK: Properties
    var type: String?
    
    var length: Int?
    
    var options: [QuestionOptionMappable]?
    
    var question: String?
    
    
    
    //MARK: Mapping
    init?(map: Map) {}
    
    init() {}
    
    mutating func mapping(map: Map) {
        type <- map["type"]
        length <- map["length"]
        options <- map["options"]
        question <- map["question"]
    }
    
}


struct QuestionOptionMappable: Mappable {
    //MARK: Properties
    var name: String?
    
    //MARK: Mapping
    init?(map: Map) { }
    
    init() {}
    
    mutating func mapping(map: Map) {
        name <- map["value"]
    }
    
}
