//
//  QuesitonsViewModel.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/22/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import Foundation

enum QuestionType: String {
    case Text = "text"
    case Date = "date"
    case Number = "number"
    case Radio = "radio"
    case File = "file"
    case Contact = "contact"
}

struct QuestionsViewModel {
    
    var questions: [QuestionViewModel]
    
    init(fromQuestions questionsModel: [Question]) {
        questions = questionsModel.map { return QuestionViewModel(fromQuestion: $0) }
    }
    
    mutating func updateQuestion(_ question: QuestionViewModel, atIndex index: Int) {
        questions[index] = question
    }
    
}

struct QuestionViewModel {
    
    var questionText: String
    
    var type: QuestionType
    
    var options: [String]
    
    var length: Int
    
    var quantity: Int
    
    var answer = ""
    
    init(fromQuestion question: Question) {
        questionText = question.question
        type = QuestionType(rawValue: question.questionType) ?? .Text
        options = question.questionOptions.map { return $0.name }
        length = question.length
        quantity = question.quantity
    }
    
    mutating func setAnswer(_ answer: String) {
        self.answer = answer
    }
    
}
