//
//  NSObjectExtension.swift
//  Visa
//
//  Created by Adrian Hernandez on 1/3/18.
//  Copyright © 2018 YellowPepper USA LLC. All rights reserved.
//

import Foundation

extension NSObject {
    
    class var className : String {
        return String(describing: self)
    }
    
    var className: String {
        return String(describing: type(of: self))
    }
    
}


