//
//  Constants.swift
//  Visa
//
//  Created by Enrique Salazar Sixtos on 06/12/17.
//  Copyright © 2017 YellowPepper USA LLC. All rights reserved.
//

import Foundation
import UIKit
import MapKit

struct Constants {
    
    //MARK: Internal Constants
    static let ASCII_backspace = -92
    static let char_backspace = "\\b"
    static let version = "Versión 1.0.0"
    static let defaultTextFieldPadding: CGFloat = 10.0
    static let defaultError = NSError(domain:"", code: 432, userInfo:nil)
    static let defaultAttemptsRemaining = 2
    static let encryptCardLength = 8
    
    //MARK: - Storyboard names
    
    
    //MARK: - Nib names
    
    
    //MARK: - ViewController identifiers
    
    
}

