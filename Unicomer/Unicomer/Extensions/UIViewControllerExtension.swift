//
//  UIViewControllerExtension.swift
//  Visa
//
//  Created by Adrian Hernandez on 12/12/17.
//  Copyright © 2017 YellowPepper USA LLC. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

extension UIViewController {
    
    /// Creates an instance of the view controller from the storyboard.
    ///
    /// - Parameters:
    ///   - name: The name of the storyboard where the view controller is defined.
    ///   - bundle: The bundle where the storyboard belongs. Default vaule is `nil` for which
    ///             the main bundle is used.
    ///   - id: The storyboard identifier for the view controller. If `nil` uses the view controller's type as the identifier. Default value is `nil`.
    /// - Returns: An instance of the view controller or `nil` if the view controller with the identifier does not exists in the storyboard.
    class func fromStoryboard(_ name: String, in bundle: Bundle? = nil, withIdentifier id: String? = nil) -> Self? {
        return fromStoryboard(UIStoryboard(name: name, bundle: bundle), withIdentifier: id)
    }
    
    
    /// Creates an instance of the view controller from the storyboard.
    ///
    /// - Parameters:
    ///   - storyboard: The storyboard where the view controller is defined.
    ///   - id: The storyboard identifier for the view controller. If `nil` uses the view controller's type as the identifier. Default value is `nil`.
    /// - Returns: An instance of the view controller or `nil` if the view controller with the identifier does not exists in the storyboard.
    class func fromStoryboard(_ storyboard: UIStoryboard, withIdentifier id: String? = nil) -> Self? {
        return fromStoryboard(storyboard, withIdentifier: id, as: self)
    }
    
    // MARK: Private
    
    private class func fromStoryboard<T>(_ storyboard: UIStoryboard, withIdentifier id: String? = nil, as type: T.Type) -> T? {
        return  storyboard.instantiateViewController(withIdentifier: id ?? "\(type)") as? T
    }
    
    //MARK: Activity Indicator
    
    public func showActivityIndicator() {
        HUD.show(.progress)
    }
    
    public func dissmissActivityIndicator() {
        HUD.hide()
    }
    
    //MARK: Alert methods
    
    func showAlert(message: String) {
        showAlert(title: "view.default.title".localized, message: message, completion: nil)
    }
    
    func showAlert(title: String, message: String, completion:(()-> Void)?) {
        let alertController = UIAlertController(title: title, message:message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "default.option.OK".localized, style: .default) { action in
            completion?()
        }
        
        alertController.addAction(defaultAction)
        present(alertController, animated: false, completion: nil)
    }
    
}
