//
//  QuestionsViewDelegateDataSource.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 3/1/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import UIKit
import ContactsUI

class QuestionsViewDelegateDataSource: NSObject, QuestionsDisplayViewDelegate, QuestionsDisplayViewDataSource {
    
    //MARK: Properties
    
    var questions: [QuestionViewModel]
    
    var presentingController: UIViewController
    
    var questionsDisplayView: QuestionsDisplayView
    
    var currentQuestion: QuestionViewModel?
    
    //MARK: Initializer
    
    init(questions: [QuestionViewModel], presentingController: UIViewController, questionsDisplayView: QuestionsDisplayView) {
        self.questions = questions
        self.presentingController = presentingController
        self.questionsDisplayView = questionsDisplayView
        super.init()
    }
    
    //MARK: Generate Answer Views
    
    private func generateAnswerView(forQuestion question: QuestionViewModel) -> AnswerView {
        switch question.type {
        case .Text, .Date, .Number:
            return TextAnswerView.CreateAnswerViewFromModel(question)
        case .Radio:
            return MultipleChocieAnswerView.CreateAnswerViewFromModel(question)
        case .File:
            let fileUploadView = FileUploadView.CreateAnswerViewFromModel(question)
            fileUploadView.delegate = self
            return fileUploadView
        case .Contact:
            let contactAnswerView = ContactsAnswerView.CreateAnswerViewFromModel(question)
            contactAnswerView.delegate = self
            return contactAnswerView
        }
    }
    
    //MARK: QuestionsDisplayView Datasource
    
    func numberOfQuestions(_ questionsDisplayView: QuestionsDisplayView) -> Int {
        return questions.count
    }
    
    func questionViewModel(_ questionsDisplayView: QuestionsDisplayView, forIndex index: Int) -> QuestionViewModel? {
        return questions[index]
    }
    
    func updateQuestion(_ questionsDisplayView: QuestionsDisplayView, question: QuestionViewModel, atIndex index: Int) {
        questions[index] = question
    }
    
    //MARK: QuestionsDisplayView Delegate
    
    func answerView(_ questionsDisplayView: QuestionsDisplayView, forQuestionModel question: QuestionViewModel) -> AnswerView? {
        return generateAnswerView(forQuestion: question)
    }
    
    func errorPresented(_ questionsDisplayView: QuestionsDisplayView, errorMessage: String) {
        presentingController.showAlert(title: "Error", message: errorMessage, completion: nil)
    }
}

extension QuestionsViewDelegateDataSource: FileUploadViewDelegate {
    
    func displayPhotosFromLibrary(forQuestion question: QuestionViewModel) {
        currentQuestion = question
        displayPickerController(withSourceType: .savedPhotosAlbum)
    }
    
    func displayCameraRoll(forQuestion question: QuestionViewModel) {
        currentQuestion = question
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            displayPickerController(withSourceType: .camera)
        } else {
            presentingController.showAlert(title: "Error", message: "Camera is not available", completion: nil)
        }
    }
    
    private func displayPickerController(withSourceType type: UIImagePickerControllerSourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = type
        presentingController.showActivityIndicator()
        presentingController.present(imagePicker, animated: true, completion: { self.presentingController.dissmissActivityIndicator() })
    }
    
}

extension QuestionsViewDelegateDataSource: ContactsAnswerViewDelegate {
    
    func displayContactsPickerController(forQuestion question: QuestionViewModel) {
        currentQuestion = question
        let contactController = CNContactPickerViewController()
        contactController.delegate = self
        presentingController.present(contactController, animated: true, completion: nil)
    }
    
}

extension QuestionsViewDelegateDataSource: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage, var question = currentQuestion else { return }
        let normalImage = image.getImageWithOrientationFixed()
        let imageData = UIImagePNGRepresentation(normalImage)!
        let imageStr = imageData.base64EncodedString()
        question.answer = imageStr
        questionsDisplayView.reloadQuestion(question: question)
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension QuestionsViewDelegateDataSource: CNContactPickerDelegate {
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        picker.dismiss(animated: true, completion: nil)
        guard let question = currentQuestion else { return }
        if contacts.count > question.quantity && question.quantity > 0 {
            presentingController.showAlert(title: "Too many contacts", message: "You may only select \(question.quantity) contacts", completion: nil)
        } else {
            updateQuestionWithContacts(contacts)
        }
    }
    
    private func updateQuestionWithContacts(_ contacts: [CNContact]) {
        guard var question = currentQuestion else { return }
        var answer = ""
        for (i, contact) in contacts.enumerated() {
            var phoneNumbers = ""
            for (index, phoneNumber) in contact.phoneNumbers.enumerated() {
                if index == 0 {
                    phoneNumbers += phoneNumber.value.stringValue
                } else {
                    phoneNumbers += ",\(phoneNumber.value.stringValue)"
                }
            }
            if i == 0 {
                answer += "\(contact.givenName) \(contact.familyName);\(phoneNumbers)"
            } else {
                answer += "\n\(contact.givenName) \(contact.familyName);\(phoneNumbers)"
            }
        }
        question.answer = answer
        questionsDisplayView.reloadQuestion(question: question)
    }
    
}
