//
//  UnicomerTextField.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/23/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import UIKit

class UnicomerTextField: UITextField {
    
    private func setup() {
        addBottomBorderWithColor(.black)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

}
