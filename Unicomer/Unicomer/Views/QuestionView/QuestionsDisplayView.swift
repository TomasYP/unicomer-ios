//
//  QuestionsDisplayView.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/22/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import UIKit

protocol QuestionsDisplayViewDataSource: class {
    func numberOfQuestions(_ questionsDisplayView: QuestionsDisplayView) -> Int
    func questionViewModel(_ questionsDisplayView: QuestionsDisplayView, forIndex index: Int) -> QuestionViewModel?
    func updateQuestion(_ questionsDisplayView: QuestionsDisplayView, question: QuestionViewModel, atIndex index: Int)
}

protocol QuestionsDisplayViewDelegate: class {
    func answerView(_ questionsDisplayView: QuestionsDisplayView, forQuestionModel question: QuestionViewModel) -> AnswerView?
    func errorPresented(_ questionsDisplayView: QuestionsDisplayView, errorMessage: String)
}

protocol AnswerView {
    var questionModel: QuestionViewModel? { get set }
    var hasFlexibleHeight: Bool { get }
    func gatherAnswerFromUserInput() throws -> String
}

enum AnswerViewError: Error {
    case insuficientData(message: String)
}

class QuestionsDisplayView: UIView {
    
    //MARK: Outlets
    
    private var questionLabel: UILabel!
    
    private var nextButton: UIButton!
    
    private var previousButton: UIButton!
    
    private var buttonStackView: UIStackView!
    
    //MARK: Properties
    
    private var currentQuestionIndex = 0
    
    private var currentQuestion: QuestionViewModel?
    
    weak var delegate: QuestionsDisplayViewDelegate?
    
    weak var dataSource: QuestionsDisplayViewDataSource?
    
    private var isInLastQuestion: Bool {
        return currentQuestionIndex >= (dataSource?.numberOfQuestions(self) ?? 0) - 1
    }
    
    private var answerView: AnswerView?
    
    //MARK: View drawing
    
    private func setupQuestionLabel() {
        questionLabel = UILabel(frame: .zero)
        addSubview(questionLabel)
        questionLabel.translatesAutoresizingMaskIntoConstraints = false
        questionLabel.numberOfLines = 0
        questionLabel.textAlignment = .center
        questionLabel.text = ""
        let topConstraint = questionLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8.0)
        let trailingConstraint = questionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 8.0)
        let leadingConstraint = questionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8.0)
        NSLayoutConstraint.activate([topConstraint, trailingConstraint, leadingConstraint])
        setNeedsLayout()
    }
    
    private func setButtons() {
        setButtonStackView()
        setupPreviousButton()
        setupNextButton()
    }
    
    private func setButtonStackView() {
        let stackViewWidth: CGFloat = 250
        buttonStackView = UIStackView(frame: CGRect(origin: .zero, size: CGSize(width: stackViewWidth, height: 50)))
        addSubview(buttonStackView)
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonStackView.axis = .horizontal
        buttonStackView.distribution = .fillEqually
        buttonStackView.spacing = 10.0
        buttonStackView.backgroundColor = .red
        let topConstraint = buttonStackView.topAnchor.constraint(greaterThanOrEqualTo: questionLabel.bottomAnchor, constant: 20.0)
        let centerXConstraint = buttonStackView.centerXAnchor.constraint(equalTo: centerXAnchor)
        let widthConstraint = buttonStackView.widthAnchor.constraint(equalToConstant: stackViewWidth)
        NSLayoutConstraint.activate([topConstraint, centerXConstraint, widthConstraint])
    }
    
    private func setupNextButton() {
        nextButton = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 200, height: 50)))
        nextButton.setTitle("Next", for: .normal)
        nextButton.tintColor = .white
        nextButton.backgroundColor = UIColor(displayP3Red: 71 / 255 , green: 168 / 255, blue: 219 / 255, alpha: 1.0)
        nextButton.isEnabled = false
        nextButton.addTarget(self, action: #selector(nextButtonTapped(sender:)), for: .touchUpInside)
        buttonStackView.addArrangedSubview(nextButton)
    }
    
    private func setupPreviousButton() {
        previousButton = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 200, height: 50)))
        previousButton.setTitle("Previous", for: .normal)
        previousButton.tintColor = .white
        previousButton.backgroundColor = UIColor(displayP3Red: 71 / 255 , green: 168 / 255, blue: 219 / 255, alpha: 1.0)
        previousButton.isEnabled = false
        previousButton.addTarget(self, action: #selector(previousButtonTapped(sender:)), for: .touchUpInside)
        buttonStackView.addArrangedSubview(previousButton)
    }
    
    private func applyContraintsForAnswerView(_ view: UIView, width: CGFloat, ignoreHeight: Bool = false) {
        view.translatesAutoresizingMaskIntoConstraints = false
        let centerXConstraint = view.centerXAnchor.constraint(equalTo: centerXAnchor)
        let widthConstraint = view.widthAnchor.constraint(equalToConstant: width)
        let topConstraint = view.topAnchor.constraint(equalTo: questionLabel.bottomAnchor, constant: 20.0)
        let bottomConstraint = view.bottomAnchor.constraint(equalTo: buttonStackView.topAnchor, constant: -20.0)
        NSLayoutConstraint.activate([centerXConstraint, widthConstraint, topConstraint, bottomConstraint])
        if !ignoreHeight {
            let heightConstraint = view.heightAnchor.constraint(equalToConstant: view.bounds.height)
            NSLayoutConstraint.activate([heightConstraint])
        }
    }
    
    func realoadView() {
        if currentQuestion == nil {
            nextButton.isEnabled = true
            currentQuestion = dataSource?.questionViewModel(self, forIndex: currentQuestionIndex)
            setViewForCurrentQuestion()
        }
    }
    
    func reloadQuestion(question: QuestionViewModel) {
        currentQuestion = question
        setViewForCurrentQuestion()
    }
    
    //MARK: Questions
    
    private func determineNextQuestion() {
        currentQuestionIndex += 1
        currentQuestion = dataSource?.questionViewModel(self, forIndex: currentQuestionIndex)
        setViewForCurrentQuestion()
    }
    
    private func determinePreviousQuestion() {
        if currentQuestionIndex > 0 {
            currentQuestionIndex -= 1
            currentQuestion = dataSource?.questionViewModel(self, forIndex: currentQuestionIndex)
            setViewForCurrentQuestion()
        }
    }

    private func setViewForCurrentQuestion() {
        guard let currentQ = currentQuestion else { return }
        UIView.animate(withDuration: 0.25) {
            self.setAlphaForControls(alpha: 0.0)
            self.addAnswerViewForQuestion(currentQ)
        }
        UIView.animate(withDuration: 0.25, delay: 0.25, animations: {
            self.questionLabel.text = currentQ.questionText
            if self.isInLastQuestion {
                self.nextButton.setTitle("Submit", for: .normal)
            } else {
                self.nextButton.setTitle("Next", for: .normal)
            }
            self.setNeedsLayout()
            self.setAlphaForControls(alpha: 1.0)
        }, completion: nil)
    }
    
    private func setAlphaForControls(alpha: CGFloat) {
        questionLabel.alpha = alpha
        buttonStackView.alpha = alpha
        (answerView as? UIView)?.alpha = alpha
    }
    
    private func addAnswerViewForQuestion(_ question: QuestionViewModel) {
        (answerView as? UIView)?.removeFromSuperview()
        answerView = delegate?.answerView(self, forQuestionModel: question)
        guard let ansVW = answerView as? UIView else { return }
        let width = bounds.insetBy(dx: bounds.width * 0.05, dy: 1.0).width
        let ignoreHeight = answerView?.hasFlexibleHeight ?? true
        ansVW.frame.size.width = width
        addSubview(ansVW)
        applyContraintsForAnswerView(ansVW, width: width, ignoreHeight: ignoreHeight)
        (answerView as? UIView)?.alpha = 0.0
    }
    
    //MARK: Actions
    
    @objc func nextButtonTapped(sender: UIButton) {
        do {
            guard let answerVw = answerView, var currentQ = currentQuestion else { return }
            previousButton.isEnabled = true
            let answer = try answerVw.gatherAnswerFromUserInput()
            currentQ.setAnswer(answer)
            dataSource?.updateQuestion(self, question: currentQ, atIndex: currentQuestionIndex)
            if !isInLastQuestion {
                determineNextQuestion()
                nextButton.isEnabled = true
            } else {
                nextButton.isEnabled = false
            }
        } catch AnswerViewError.insuficientData(let message) {
            delegate?.errorPresented(self, errorMessage: message)
        } catch {
            delegate?.errorPresented(self, errorMessage: "An unknown error ocurred. Please try again in a few seconds")
        }
    }
    
    @objc func previousButtonTapped(sender: UIButton) {
        determinePreviousQuestion()
        if currentQuestionIndex == 0 {
            previousButton.isEnabled = false
        }
        if !isInLastQuestion {
            nextButton.isEnabled = true
        }
    }
    
    //MARK: View lifecycle
    
    private func setup() {
        backgroundColor = .clear
        isOpaque = false
        translatesAutoresizingMaskIntoConstraints = false
        setupQuestionLabel()
        setButtons()
        setNeedsLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
}
