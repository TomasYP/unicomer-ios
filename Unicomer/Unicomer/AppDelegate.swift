//
//  AppDelegate.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/21/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.sharedManager().enable = true
        showInitialViewController()
        return true
    }

    private func showInitialViewController() {
        guard let controller = HomeViewController.fromStoryboard("Questions", in: nil, withIdentifier: HomeViewController.className) else { return }
        let navController = UINavigationController(rootViewController: controller)
        navController.navigationBar.barStyle = .default
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }

}

