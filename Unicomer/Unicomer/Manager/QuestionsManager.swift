//
//  QuestionsManager.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/22/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import Foundation
import Alamofire

class QuestionsManager {
    
    //MARK: Properties
    
    static let shared = QuestionsManager()
    
    var questions: [Question] = []
    
    //MARK: Questions Loading
    
    func loadQuestions (callback: @escaping (Bool, [Question]) -> ()) {
        Alamofire.request("http://demo0740180.mockable.io/mockQuestion").responseJSON { response in
            if let json = response.result.value as? [String: AnyObject] {
                guard let questionResponse = QuestionsResponseMappable(JSON: json) else {
                    callback(false, self.questions)
                    return
                }
                for questionMappable in questionResponse.questions ?? [] {
                    self.questions.append(Question(fromMappable: questionMappable))
                }
                callback(true, self.questions)
            } else {
                callback(false, self.questions)
            }
        }
    }
    
}


extension QuestionsManager {
    
    func mockQuestions() -> String {
        return """
            {
                \"questions\" : [
                {
                    \"type\": \"Contact\",
                    \"question\": \"Provide emergency contact information.\",
                    \"quantity\": 3
                },
                {
                    \"type\": \"File\",
                    \"question\": \"Provide a copy of your id card.\"
                },
                {
                    \"type\": \"Radio\",
                    \"question\": \"Are you married?\",
                    \"options\": [
                    {
                        \"name\": \"Yes\"
                    },
                    {
                        \"name\": \"No\"
                    },
                    {
                        \"name\": \"Engaged\"
                    },
                    {
                        \"name\": \"Very incredibly long long long long answer. Testing how long options will lok like!\"
                    }
                    ]
                },
                {
                    \"type\": \"Text\",
                    \"question\": \"What is your name?\",
                    \"format\": \"Text\",
                    \"length\": 15
                },
                {
                    \"type\": \"Text\",
                    \"question\": \"What's your age?\",
                    \"format\": \"Currency\",
                    \"length\": 50
                },
                {
                    \"type\": \"Text\",
                    \"question\": \"This is a really long text to test multiline questions. Now that we have cleared that. What is your birth date?\",
                    \"format\": \"Date\"
                }
                ]
            }
        """
    }
    
}
