//
//  MultilineTextField.swift
//  Visa
//
//  Created by Adrian Hernandez on 1/10/18.
//  Copyright © 2018 YellowPepper USA LLC. All rights reserved.
//

import UIKit

private struct TextViewConstants {
    static let borderWidth: CGFloat = 0.5
    static let cornerRaidus: CGFloat = 5
    static let placeholderHorizontalPadding: CGFloat = 7
    static let textContainerLeftInset: CGFloat = 3
    static let textContainerRightInset: CGFloat = 0
    static let growingAnimationDuration: TimeInterval = 0.2
    static var borderColor : UIColor {
       return UIColor(red: 199.0 / 255.0, green: 199.0 / 255.0, blue: 205.0 / 255.0, alpha: 1.0)
    }
    static var placeholderColor: UIColor {
         return UIColor(red: 199.0 / 255.0, green: 199.0 / 255.0, blue: 205.0 / 255.0, alpha: 1.0)
    }
}

enum YPMultilineTextBorderStyle : Int {
    case none
    case roundedRect
}

enum YPMultilineTextFieldGrowMode : Int {
    case up
    case down
}

enum YPMultilineTextFieldTextVerticalAlignment {
    case top
    case center
}


@objc protocol YPMultilinesTextFieldDelegate: UITextViewDelegate {
    func multipleLinesTextField(_ textField: YPMultilinesTextField, didChangeContentSize newContentSize: CGSize)
}

/**
 Inherits from UITextView but mimics the behavior of a UITextField.
 Has a placeholder and attributedPlaceholder properities.
 Supports rounded borders.
 Supports multiple lines.
 When used in Interface Builder, add a constraint for the text view height, and link it to the heightConstraint property. YPMultilinesTextField uses heightConstraint to update it's frame so the text fits inside.
 */
@IBDesignable
class YPMultilinesTextField: UITextView {
    
    @IBInspectable var placeholder: String? {
        didSet { setNeedsDisplay() }
    }
    @IBInspectable var shouldGrowAutomatically = false
    
    /**
     Whether the text field should animate changes in the height. Default value is YES.
     */
    @IBInspectable var animateHeightChange = true
    
    #if TARGET_INTERFACE_BUILDER
    @IBInspectable var borderStyle: Int = 0 {
        didSet { configure(with: borderStyle) }
    }
    #else
    var borderStyle: YPMultilineTextBorderStyle = .none {
        didSet { configure(with: borderStyle) }
    }
    #endif
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint? {
        didSet { updateHeight(toMatchContentSize: contentSize, animated: animateHeightChange) }
    }
   
    var attributedPlaceholder: NSAttributedString?
    weak var multilinesFieldDelegate: YPMultilinesTextFieldDelegate?
    
    var growMode: YPMultilineTextFieldGrowMode = .down
    var textVerticalAlignment : YPMultilineTextFieldTextVerticalAlignment = .top {
        didSet {
            configureTextContainerInsets()
            setNeedsDisplay()
        }
    }
    
    override var attributedText: NSAttributedString? {
        didSet { setNeedsDisplay() }
    }
    
    override var textAlignment: NSTextAlignment {
        didSet { setNeedsDisplay() }
    }
    
    override var font: UIFont? {
        didSet {
            setNeedsDisplay()
            configureTextContainerInsets()
        }
    }
    
    override var text: String! {
        didSet {
            setNeedsDisplay()
            layoutIfNeeded()
        }
    }
    
    override var contentSize: CGSize {
        didSet {
            if shouldGrowAutomatically {
                updateHeight(toMatchContentSize: contentSize, animated: animateHeightChange)
            }
            notifyContentSizeChanged(contentSize)
        }
    }
    
    private var initialFrame : CGRect = CGRect.zero
    
    private var currentPlaceholder : NSAttributedString? {
        if let attributedPlaceholder = attributedPlaceholder {
            return attributedPlaceholder.copy() as? NSAttributedString
        }
        if let placeholder = placeholder{
            return NSAttributedString(string: placeholder, attributes: defaultPlaceholderAttributes())
        }
        return nil
    }
    
    //MARK: - View Life Cycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonConfiguration()
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        commonConfiguration()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        configurePlaceholder(in: rect)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
    }
    
    override func prepareForInterfaceBuilder() {
        updateHeight(toMatchContentSize: contentSize, animated: false)
        configure(with: borderStyle)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("*** DEALOCATING: \(self) ***")
    }
    
    // MARK: - Configuration
    
    func commonConfiguration() {
        initialFrame = frame
        registerForTextChangesNotifications()
        setUpUI()
    }
    
    func setUpUI() {
        configureTextContainerInsets()
    }
    
    func configurePlaceholder(in rect: CGRect) {
        if let placeholder = currentPlaceholder, (text ?? "") == "" {
            let placeholderSize = placeholder.boundingRect(with: CGSize(width: rect.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil).size
            let drawingRectSize = CGSize(width: rect.size.width, height: placeholderSize.height)
            let drawingRect = CGRect(x: TextViewConstants.placeholderHorizontalPadding,
                                     y: textContainerInset.top,
                                     width: drawingRectSize.width,
                                     height: drawingRectSize.height)
            placeholder.draw(in: drawingRect)
        }
    }

    func defaultPlaceholderAttributes() -> [NSAttributedStringKey: Any] {
        let color: UIColor = TextViewConstants.placeholderColor
        var attributes : [NSAttributedStringKey: Any] = [NSAttributedStringKey.foregroundColor: color]
        if let font = font {
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 1.0
            style.maximumLineHeight = font.pointSize
            attributes[NSAttributedStringKey.paragraphStyle] = style
            attributes[NSAttributedStringKey.font] = font
        }
        return attributes
    }
    
    func configureTextContainerInsets() {
        if textVerticalAlignment == .center {
            let fontSize = font?.pointSize ?? UIFont.systemFontSize
            let height: CGFloat = initialFrame.size.height
            let verticalInset: CGFloat = (height - fontSize) / 2
            textContainerInset = UIEdgeInsetsMake(verticalInset, TextViewConstants.textContainerLeftInset, verticalInset, TextViewConstants.textContainerRightInset)
        }
    }
    
    // MARK: - Border Style
    func configure(with borderStyle: YPMultilineTextBorderStyle) {
        if borderStyle == .roundedRect {
            layer.borderWidth  = TextViewConstants.borderWidth
            layer.borderColor  = TextViewConstants.borderColor.cgColor
            layer.cornerRadius = TextViewConstants.cornerRaidus
        }
        else if borderStyle == .none {
            layer.borderWidth = 0.0
            layer.borderColor = UIColor.clear.cgColor
            layer.cornerRadius = 0.0
        }
        
    }
    
    // MARK: - Helpers
    func updateHeight(toMatchContentSize contentSize: CGSize, animated: Bool) {
        
        if let heightConstraint = heightConstraint {
            heightConstraint.constant = contentSize.height
            if animated {
                UIView.animate(withDuration: TextViewConstants.growingAnimationDuration, animations: {() -> Void in
                    self.superview?.layoutIfNeeded()
                })
            }
            else {
                superview?.layoutIfNeeded()
            }
            
        } else {
            // update frame
            var newY: CGFloat = self.y - (contentSize.height - self.height)
            if growMode == .down {
                newY = self.y
            }
            if animated {
                UIView.animate(withDuration: TextViewConstants.growingAnimationDuration, animations: {() -> Void in
                    self.frame = CGRect(x: self.frame.origin.x, y: newY, width: contentSize.width, height: contentSize.height)
                })
            }
            else {
                frame = CGRect(x: frame.origin.x, y: newY, width: contentSize.width, height: contentSize.height)
            }
        }
    }
    
    func notifyContentSizeChanged(_ contentSize: CGSize) {
        multilinesFieldDelegate?.multipleLinesTextField(self, didChangeContentSize: contentSize)
    }
}

extension YPMultilinesTextField {
    func registerForTextChangesNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleTextChange(_:)), name: .UITextViewTextDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleTextChange(_:)), name: .UITextViewTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleTextChange(_:)), name: .UITextViewTextDidEndEditing, object: nil)
    }
    
    @objc func handleTextChange(_ notification: Notification) {
        setNeedsDisplay()
    }
}

