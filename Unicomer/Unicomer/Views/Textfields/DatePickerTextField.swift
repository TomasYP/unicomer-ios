
import UIKit

class DatePickerTextField: UITextField {
    
    private var datePicker: UIDatePicker!
    
    var date: Date {
        get {
            return datePicker.date
        }
        set {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            text = dateFormatter.string(from: newValue)
            datePicker.date = newValue
        }
    }
    
    //MARK: Actions
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        text = dateFormatter.string(from: datePicker.date)
    }
    
    @objc func doneButtonPressed(_: UIBarButtonItem) {
        self.resignFirstResponder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        text = dateFormatter.string(from: datePicker.date)
    }
    
    //MARK: Datepicker settings
    
    func define(maximumDate: Date?, minimumDate: Date?) {
        if let maxDate = maximumDate {
            datePicker.maximumDate = maxDate
        }
        
        if let minDate = minimumDate {
            datePicker.minimumDate = minDate
        }
    }
    
    //MARK: Setup
    
    private func setupInputs() {
        setupInputView()
        addBottomBorderWithColor(.black)
    }
    
    private func setupInputView() {
        self.datePicker = UIDatePicker.init(frame: CGRect.zero)
        self.datePicker.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        self.datePicker.datePickerMode = .date
        self.inputView = datePicker
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInputs()
        textAlignment = .center
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupInputs()
    }

}
