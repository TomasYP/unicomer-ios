//
//  UIColorExtension.swift
//  Visa
//
//  Created by Enrique Salazar Sixtos on 06/12/17.
//  Copyright © 2017 YellowPepper USA LLC. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class var bbvaTintColor : UIColor {
        return UIColor(red: 0/255, green: 35/255, blue: 84/255, alpha: 1)
    }
    
    class var bbvaTextFieldToolBarColor : UIColor {
        return UIColor(red: 21/255, green: 126/255, blue: 251/255, alpha: 1)
    }

    class var bbvaInactiveButtonBackgroundColor : UIColor {
        return UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1)
    }
    
    class var bbvaActiveButtonBackgroundColor : UIColor {
        return UIColor(red: 0/255, green: 131/255, blue: 218/255, alpha: 1)
    }
    
    class var customToolBarDefaultTintColor : UIColor {
        return UIColor(red: 21/255, green: 126/255, blue: 251/255, alpha: 1)
    }
    
    class var bbvaTripFormTextFieldBackgroundColor : UIColor {
        return UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
    }
    
    class var bbvaTripFormTextFieldTextColor : UIColor {
        return UIColor(red: 81/255, green: 81/255, blue: 81/255, alpha: 1)
    }
    
    class var bbvaTripFormTextFieldTitleColor : UIColor {
        return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.45)
    }

    class var bbvaAlertBackgroundColor: UIColor {
        return UIColor(red: 226/255, green: 226/255, blue: 226/255, alpha: 1)
    }
    
    class var bbvaTextDarkBlue: UIColor {
        return UIColor(red: 0/255, green: 35/255, blue: 84/255, alpha: 1)
    }
    
    class var bbvaTextGray: UIColor {
        return UIColor(red: 171/255, green: 171/255, blue: 171/255, alpha: 1)
    }
    
    class var bbvaTextDarkGray: UIColor {
        return UIColor(red: 81/255, green: 81/255, blue: 81/255, alpha: 1)
    }
    
    class var bbvaTripCellTitleDarkBlue: UIColor {
        return UIColor(red: 0/255, green: 63/255, blue: 123/255, alpha: 1)
    }
    
    class var bbvaTripCellDescriptionBlack: UIColor {
        return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
    }
    
    class var bbvaCurleanTwo: UIColor {
        return UIColor(red: 0/255, green: 141/255, blue: 216/255, alpha: 1)
    }
    
    class var bbvaLightGreen: UIColor {
        return UIColor(red: 0/255, green: 205/255, blue: 186/255, alpha: 1)
    }
}
