//
//  TextAnswerView.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/22/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import UIKit

class TextAnswerView: UIView, AnswerView {

    //MARK: Outlets
    
    private var multiTextField: YPMultilinesTextField!
    
    private var textField: UnicomerTextField!
    
    private var dateTextfield: DatePickerTextField!
    
    private var warninigLabel: UILabel!
    
    //MARK: Properties
    
    var questionModel: QuestionViewModel? {
        didSet {
            addViewsAccordingToFormat()
        }
    }
    
    var hasFlexibleHeight: Bool {
        return false
    }
    
    //MARK: Constructor
    
    class func CreateAnswerViewFromModel(_ model: QuestionViewModel) -> TextAnswerView {
        let textAnswerView = TextAnswerView(frame: CGRect(origin: .zero, size: CGSize(width: 0.0, height: 150)))
        textAnswerView.questionModel = model
        return textAnswerView
    }
    
    //MARK: Drawing
    
    private func addViewsAccordingToFormat() {
        addWarningLabel()
        guard let type  = questionModel?.type else { return }
        switch  type {
        case .Text:
            setupMultilineTextField()
        case .Number:
            setupTextField(keyboardType: .decimalPad)
        case .Date:
            setupDatepickerField()
        default:
            print("Question should not have that format")
        }
    }
    
    private func addWarningLabel() {
        warninigLabel = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: bounds.width, height: 50)))
        addSubview(warninigLabel)
        warninigLabel.text = ""
        warninigLabel.textColor = .red
        warninigLabel.textAlignment = .center
        warninigLabel.numberOfLines = 0
        warninigLabel.alpha = 0.0
        warninigLabel.translatesAutoresizingMaskIntoConstraints = false
    }
    
    fileprivate func showWarning(_ message: String) {
        warninigLabel.text = message
        warninigLabel.alpha = 1.0
    }
    
    private func setupTextField(keyboardType: UIKeyboardType = .default) {
        textField = UnicomerTextField(frame: CGRect(origin: .zero, size: CGSize(width: bounds.width, height: 50)))
        addSubview(textField)
        textField.placeholder = "Write your answer here"
        textField.keyboardType = keyboardType
        textField.textAlignment = .center

        if let answer = questionModel?.answer {
            textField.text = answer
        }
        textField.delegate = self
        applyConstraintsToField(textField)
    }
    
    private func setupMultilineTextField() {
        multiTextField = YPMultilinesTextField(frame: CGRect(origin: .zero, size: CGSize(width: bounds.width, height: 50)))
        addSubview(multiTextField)
        multiTextField.placeholder = "Write your answer here"
        multiTextField.shouldGrowAutomatically = true
        multiTextField.configure(with: .roundedRect)
        multiTextField.multilinesFieldDelegate = self
        applyConstraintsToField(multiTextField)
        if let answer = questionModel?.answer {
            multiTextField.text = answer
        }
//        multiTextField.heightConstraint = multiTextField.heightAnchor.constraint(equalToConstant: 50)
        multiTextField.setNeedsLayout()
    }
    
    private func setupDatepickerField() {
        let datePickerTextfield = DatePickerTextField(frame: CGRect(origin: .zero, size: CGSize(width: bounds.width, height: 50)))
        addSubview(datePickerTextfield)
        dateTextfield = datePickerTextfield
        dateTextfield.placeholder = "Write your answer here"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        if let answer = questionModel?.answer, answer != "", let date = dateFormatter.date(from: answer) {
            dateTextfield.date = date
        }
        applyConstraintsToField(datePickerTextfield)
    }
    
    private func applyConstraintsToField(_ view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        let centerYConstraint = view.centerYAnchor.constraint(equalTo: centerYAnchor)
        let trailingConstraint = view.trailingAnchor.constraint(equalTo: trailingAnchor)
        let leadingConstraint = view.leadingAnchor.constraint(equalTo: leadingAnchor)
        NSLayoutConstraint.activate([centerYConstraint, trailingConstraint, leadingConstraint])
        let topConstraint = warninigLabel.topAnchor.constraint(equalTo: view.bottomAnchor, constant: 8.0)
        let centerXconstraint = warninigLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        let widthConstraint = warninigLabel.widthAnchor.constraint(equalTo: widthAnchor)
        NSLayoutConstraint.activate([topConstraint, centerXconstraint, widthConstraint])
        setNeedsLayout()
    }
    
    //MARK: View lifecycle
    
    private func setup() {
        backgroundColor = .clear
        isOpaque = false
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    //MARK: Answer view methods
    
    func getInput() -> String {
        guard let type  = questionModel?.type else { return "" }
        switch  type {
        case .Text:
            return multiTextField.text
        case .Number:
            return textField.text ?? ""
        case .Date:
            return "\(dateTextfield.date)"
        default:
            return ""
        }
    }
    
    func gatherAnswerFromUserInput() throws -> String {
        let answer = getInput()
        if answer == "" {
            throw AnswerViewError.insuficientData(message: "This field can't be blank in order to move on.")
        } else {
            return answer
        }
    }

}

extension TextAnswerView: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let length = questionModel?.length, length > 0,
                string != "", let text = textField.text else { return true }
        if text.count >= length {
            showWarning("Your answer can't be more than \(length) characters")
            return false
        }
        return true
    }
    
}

extension TextAnswerView: YPMultilinesTextFieldDelegate {
    
    func multipleLinesTextField(_ textField: YPMultilinesTextField, didChangeContentSize newContentSize: CGSize) {
        if multiTextField.frame.origin.y + newContentSize.height >= bounds.height - 8 {
            textField.shouldGrowAutomatically = false
        }
    }
    
}
