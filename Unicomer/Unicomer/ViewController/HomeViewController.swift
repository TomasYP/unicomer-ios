//
//  HomeViewController.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/23/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBAction private func showQuestionsTapped(_ sender: Any) {
        guard let homeViewController = QuestionsViewController.fromStoryboard("Questions", withIdentifier: QuestionsViewController.className)
            else { return }
        navigationController?.pushViewController(homeViewController, animated: true)
    }
    
}
