//
//  MultipleChocieAnswerView.swift
//  Unicomer
//
//  Created by Tomas Trujillo on 2/23/18.
//  Copyright © 2018 YellowPepper. All rights reserved.
//

import UIKit

class MultipleChocieAnswerView: UIView, AnswerView {

    //MARK: Outlets
    
    private var optionSwitches: [UISwitch] = []
    
    //MARK: Properties
    
    var questionModel: QuestionViewModel? {
        didSet {
            arrangeSwitchViews()
        }
    }
    
    var hasFlexibleHeight: Bool {
        return true
    }
    
    //MARK: Constructor
    
    class func CreateAnswerViewFromModel(_ model: QuestionViewModel) -> MultipleChocieAnswerView {
        let multipleChoiceAnswerView = MultipleChocieAnswerView(frame: CGRect(origin: .zero, size: CGSize(width: 0.0, height: 0)))
        multipleChoiceAnswerView.questionModel = model
        return multipleChoiceAnswerView
    }
    
    //MARK: Drawing
    
    private func arrangeSwitchViews() {
        guard let model = questionModel else { return }
        let standardSwitchSize = CGSize(width: 50.0, height: 30)
        var previousAnchor = topAnchor
        for (index, option) in model.options.enumerated() {
            let optionSwitch = UISwitch(frame: CGRect(origin: .zero, size: standardSwitchSize))
            optionSwitch.isOn = false
            optionSwitch.tag = index
            optionSwitch.addTarget(self, action: #selector(switchSelected(switchSelected:)), for: .touchUpInside)
            if model.answer == option {
                optionSwitch.isOn = true
            }
            let optionLabel = UILabel()
            optionLabel.numberOfLines = 0
            optionLabel.text = option
            let stackView = addToViewAndSetConstraintsFor(optionSwitch: optionSwitch, optionLabel: optionLabel, anchorOnTopOfView: previousAnchor)
            previousAnchor = stackView.bottomAnchor
            optionSwitches.append(optionSwitch)
        }
        NSLayoutConstraint.activate([bottomAnchor.constraint(greaterThanOrEqualTo: previousAnchor, constant: 8.0)])
        setNeedsLayout()
    }
    
    private func addToViewAndSetConstraintsFor(optionSwitch: UISwitch, optionLabel: UILabel, anchorOnTopOfView: NSLayoutYAxisAnchor) -> UIStackView {
        let stackView = UIStackView(arrangedSubviews: [optionSwitch, optionLabel])
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        addSubview(stackView)
//        optionLabel.translatesAutoresizingMaskIntoConstraints = false
//        optionSwitch.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        let topStackConstraint = stackView.topAnchor.constraint(equalTo: anchorOnTopOfView, constant: 8.0)
        let trailingStackConstraint = stackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        let leadingStackConstraint = stackView.leadingAnchor.constraint(equalTo: leadingAnchor)
        NSLayoutConstraint.activate([topStackConstraint, trailingStackConstraint, leadingStackConstraint])
        return stackView
        
    }
    
    //MARK: Actions
    
    @objc func switchSelected(switchSelected: UISwitch) {
        if switchSelected.isOn {
            for switchOption in optionSwitches {
                if switchOption.tag != switchSelected.tag {
                    switchOption.isOn = false
                }
            }
        }
    }
    
    //MARK: View lifecycle
    
    private func setup() {
        backgroundColor = .clear
        isOpaque = false
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    //MARK: Answer view methods
    
    private func getAnswer() -> (hasAnswered: Bool, selectedIndex: Int) {
        for (index, option) in optionSwitches.enumerated() {
            if option.isOn {
                return (true, index)
            }
        }
        return (false, -1)
    }
    
    func gatherAnswerFromUserInput() throws -> String {
        let answer = getAnswer()
        if !answer.hasAnswered {
            throw AnswerViewError.insuficientData(message: "You need to select at least one option")
        } else {
            return questionModel?.options[answer.selectedIndex] ?? ""
        }
    }

}
