//
//  StringExtension.swift
//  Visa
//
//  Created by Enrique Salazar Sixtos on 30/11/17.
//  Copyright © 2017 YellowPepper USA LLC. All rights reserved.
//

import Foundation

extension String {
    
    // this function is used to obtain the Localizable.strings
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    // this function is used to get the Localizable.strings when they have extra parameters
    func localizeWith(arguments: CVarArg...) -> String {
        let localizedValue = self.localized
        return String(format: localizedValue, arguments: arguments)
    }
    
    // this functions iare used to insert separators in the strings, the separator and the separation interval are entered.
    mutating func insert(separator: String, every interval: Int) {
        self = inserting(separator: separator, every: interval)
    }
    
    private func inserting(separator: String, every interval: Int) -> String {
        var formatedString: String = ""
        let characters = Array(self)
        guard interval != 0 else {
            return formatedString
        }
        stride(from: 0, to: characters.count, by: interval).forEach {
            formatedString += String(characters[$0..<min($0 + interval, characters.count)])
            if $0 + interval < characters.count {
                formatedString += separator
            }
        }
        return formatedString
    }
    
    //MARK: Formatting
    
    func maskedCardString()-> String? {
        let cleanCardNumber = self.replacingOccurrences(of: "-", with: "", options: .literal)
        if (cleanCardNumber.count > 10) {
            let last4: String = subString(from: cleanCardNumber.count-4)
            let first6: String = subString(to: 6)
            
            var maskLenght: Int = cleanCardNumber.count - 10
            var mask = ""
            while (maskLenght > 0) {
                mask.append("*")
                maskLenght = maskLenght-1
            }
            return String(format:"%@%@%@", first6, mask, last4)
        } else {
            return nil
        }
    }
    
    func subString(to: Int)-> String {
        if to <= count {
            let index = self.index(startIndex, offsetBy: to)
            let mySubstring = prefix(upTo: index)
            return String(mySubstring)
        } else {
            return self
        }
    }
    
    func subString(from: Int)-> String {
        if from < count {
            let index = self.index(endIndex, offsetBy:(from - count))
            let mySubstring = suffix(from: index)
            return String(mySubstring)
        } else {
            return self
        }
    }
    
}
