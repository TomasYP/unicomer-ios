//
//  UIViewExtension.swift
//  Visa
//
//  Created by Adrian Hernandez on 1/9/18.
//  Copyright © 2018 YellowPepper USA LLC. All rights reserved.
//

import UIKit

extension UIView {
    
    var height : CGFloat {
        set {
            frame.size.height = newValue
        }
        get {
            return frame.size.height
        }
    }
    
    var width : CGFloat {
        set {
            frame.size.width = newValue
        }
        get {
            return frame.size.width
        }
    }
    
    var size : CGSize {
        get { return frame.size }
        set { frame.size = CGSize(width: newValue.width, height: newValue.height) }
    }
    
    var x : CGFloat {
        get { return frame.origin.x }
        set { frame.origin.x = newValue }
    }
    
    var y : CGFloat {
        get { return frame.origin.y }
        set { frame.origin.y = newValue }
    }
    
    var yCenter : CGFloat {
        get { return self.y + self.height/2.0 }
        set { y = newValue - height/2.0 }
    }
    
    var xCenter : CGFloat {
        get { return self.x + width/2.0 }
        set { x = newValue - width/2.0 }
    }
    
    func addBottomBorderWithColor(_ color: UIColor) {
        let bottomBar = UIView(frame: CGRect(origin: .zero, size: CGSize(width: bounds.width, height: 1.0)))
        bottomBar.backgroundColor = color
        bottomBar.translatesAutoresizingMaskIntoConstraints = false
        addSubview(bottomBar)
        let topConstraint = bottomBar.topAnchor.constraint(equalTo: bottomAnchor, constant: 1.0)
        let widthConstraint = bottomBar.widthAnchor.constraint(equalTo: widthAnchor)
        let heightConstraint = bottomBar.heightAnchor.constraint(equalToConstant: 1.0)
        let centerXConstraint = bottomBar.centerXAnchor.constraint(equalTo: centerXAnchor)
        NSLayoutConstraint.activate([topConstraint, widthConstraint, heightConstraint, centerXConstraint])
        setNeedsLayout()
    }
    
}
