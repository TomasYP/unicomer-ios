//
//  UIImageViewExtension.swift
//  Visa
//
//  Created by Enrique Salazar on 10/01/18.
//  Copyright © 2018 YellowPepper USA LLC. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func makeShadowWithParams(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offSet
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
}
